const quizes = angular.module("quizes", [, "quizes.service"]);

angular
  .module("quizes")
  .controller("QuizesPageCtrl", ($scope, QuizesService) => {
    const vm = ($scope.quizesPage = {});
    //console.log('Hello QuizesPageCtrl', QuizesService)

    vm.selected = false;
    vm.selectedQuestion = false;
    vm.selectedAnswer = false;
    vm.changeAnswer = false;
    vm.quizId = "";
    vm.edit = () => {
      vm.mode = "edit";
    };

    vm.creatNew = () => {
      vm.mode = "edit";
      vm.formMode = "new";
      vm.selected = true;
      vm.changeAnswer = false;
      //console.log(vm.formMode);console.log(vm.formMode);
    };

    vm.select = (id) => {
     // console.log("select");
      vm.selectedQuiz = QuizesService.getQuizById(id);
      //console.log('vm.selectedQuiz');console.log(vm.selectedQuiz);
      vm.quizId = id;
      //console.log(id)
      vm.refreshQuestion(vm.quizId);
      //console.log('vm.questions');console.log(vm.questions)

      //vm.questionsTemp = { ...vm.questions };
      //console.log('vm.questionsTemp');console.log(vm.questionsTemp);
      vm.selected = true;
      vm.mode = "show";
      vm.formMode = "edit";
      vm.selectedQuestion = false;
      vm.selectedAnswer = false;
      //console.log(id);

      // console.log(vm.formMode)

      //console.log(id)
      //console.log($scope.selected)
    };

    vm.save = (draft) => {
      //console.log(draft);
      //console.log(draft.id);
      if (draft.id) {
        //console.log(draft.id);
        QuizesService.update(draft);
      } else {
        draft.id = vm.quizes.length + 1;
        //console.log(draft.id);
        QuizesService.addNew(draft);
      }
      vm.selected = false;
      vm.refresh();
    };
    vm.saveQuestion = (draft) => {
      console.log(draft);

      if (draft.id) {
        //console.log(draft.id);
        QuizesService.updateQuestion(draft);
      } else {
        //console.log(draft.id);
        draft.id = vm.questions.length*3 + Math.floor(Math.random() * 1000);
        draft.id_quiz = vm.quizId;
        //console.log(draft)
        QuizesService.addNewQuestion(draft);
      }
      vm.refresh();
      vm.refreshQuestion(vm.quizId);
    };
    vm.addNewQuestion = () => {
      // console.log(' addNewQuestion => vm.questions');console.log(vm.questions)
      let tempQuestion = {
        id: null,
        id_quiz: vm.quizId,
        name: "",
      };
      vm.selectedQuestion = true;
      vm.questionsTemp = { ...tempQuestion };
      vm.refreshQuestion(vm.quizId);
      vm.selectedAnswer = false;
      //vm.refreshQuestion(vm.quizId)
      // console.log(' addNewQuestion => tempQuestion');console.log(tempQuestion)
    };

    vm.refresh = () => {
      QuizesService.fetchQuizes().then((data) => {
        vm.quizes = data;
        // console.log('vm.refresh => vm.quizes');console.log(vm.quizes)
      });
    };
    vm.refreshQuestion = (id) => {
      //console.log(id);
      QuizesService.fetchQuestions(id).then((data) => {
        vm.questions = data;
        vm.questionsTemp = data.filter((item) => item.id_quiz == id);
        //console.log('refreshQuestion : vm.questions');console.log(vm.questions);
      });
    };
    vm.refreshAnswers = (id) => {
     // console.log("vm.refreshAnswers : id");
      console.log(id);
      QuizesService.fetchAnswers(id).then((data) => {
        vm.answers = data;
      //  console.log("vm.refreshAnswers :vm.questions");
      //  console.log(vm.questions);
        vm.answersTemp = data.filter((item) => item.id_question == id);
       // console.log("vm.refreshAnswers -> vm.answersTemp");
       // console.log(vm.answersTemp);
      });
    };

    vm.refreshQuestion(vm.quizId);
    vm.refresh();

    vm.selectQuestion = (draft) => {
      //console.log('selectQuestion');console.log(draft);
      vm.selectedQuestionTemp = { ...draft };
      //console.log('vm.selectQuestion -> vm.questionsTemp'); console.log(vm.questionsTemp)
      vm.refreshQuestion(vm.quizId);
      vm.answerId=draft.id
      vm.refreshAnswers(draft.id);
      // console.log('vm.answers');console.log(vm.answers)
      vm.selectedQuestion = true;
      vm.selectedAnswer = true;
      //vm.selectedQuestionTemp = question
      //console.log(vm.selectedQuestionTemp);
    };
    vm.editAnswer = (draft) => {
      //console.log("editAnswer => draft");console.log(draft);
      vm.selectedAnswerTemp = {...draft}
      //console.log("editAnswer => vm.selectedAnswerTemp");console.log(vm.selectedAnswerTemp);
      vm.changeAnswer = true;
    };
    vm.creatNewAnswer = (id) => {
     // console.log("creatNewAnswer => id");console.log(id);
      vm.changeAnswer = true;
    };
    vm.saveAnswer = (draft) => {
      //console.log('vm.saveAnswers => draft'); console.log(draft);

      if (draft.id) {
        //console.log(draft.id);
        QuizesService.updateAnswer(draft);
      } else {
        //console.log(draft.id);
        draft.id = vm.questions.length*3 + Math.floor(Math.random() * 1000);
        draft.id_question =vm.answerId;
        console.log('draft => saveAnswer');console.log(draft)
        QuizesService.addNewAnswer(draft);
      }
      vm.refresh();
      vm.refreshQuestion(vm.quizId);
      vm.refreshAnswers(vm.answerId)
    }
  });
