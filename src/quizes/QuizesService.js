angular.module("quizes.service", []).service("QuizesService", function ($http) {
  this.update = (draft) => {
    //console.log(draft);
    //console.log(draft.id);
    return $http.put("http://localhost:3000/quiz/" + draft.id, draft);
  };
  this.getQuizById = (id) => {
    return this._quizes.find((u) => u.id == id);
  };
  this.fetchQuizes = () => {
    return $http.get("http://localhost:3000/quiz").then((res) => {
      return (this._quizes = res.data);
    });
  };

  this.addNew = (draft) => {
    //console.log(draft);
    return $http.post("http://localhost:3000/quiz/", draft);
  };

  /* QUESTIONS */
  this.fetchQuestions = (id) => {
    //console.log(id)
    return (
      $http
        // .get("http://localhost:3000/questions?id_quiz=" + id)
        .get("http://localhost:3000/questions")
        .then((res) => {
          return (this._questions = res.data);
        })
    );
  };
  this.updateQuestion = (draft) => {
    // console.log(draft);
    // console.log(draft.id);
    return $http.put("http://localhost:3000/questions/" + draft.id, draft);
  };
  this.addNewQuestion = (draft) => {
    //console.log(draft);
    return $http.post("http://localhost:3000/questions/", draft);
  };
  /*ANSWERS */
  this.fetchAnswers = (id) => {
    //console.log(id)
    return (
      $http
        //.get("http://localhost:3000/answers?id_question=" + id)
        .get("http://localhost:3000/answers")
        .then((res) => {
          return (this._answers = res.data);
        })
    );
  };
  this.updateAnswer = (draft) => {
    // console.log(draft);
    // console.log(draft.id);
    return $http.put("http://localhost:3000/answers/" + draft.id, draft);
  };
  this.addNewAnswer = (draft) => {
    console.log('addNewAnswer => draft');console.log(draft);
    return $http.post("http://localhost:3000/answers/", draft);
  };
});
