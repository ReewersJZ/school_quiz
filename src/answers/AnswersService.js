// SERVICE
angular.module("answersService", []).service("AnswersService", function ($q, $http) {
  
    this.fetchQuizes = () => {
        return $http.get("http://localhost:3000/quiz")
          .then((response) => {
            return (this._quizes  = response.data)
          })
        }

    this.getQuizById = (id) => {
        const quiz = this._quizes.find((i) => i.id === id);
        return quiz;
      };

    this.fetchQuestions = () => {
    return $http.get("http://localhost:3000/questions")
        .then((response) => {
        return (this._questions  = response.data)
        })
    }
    this.fetchQuestions();

    this.fetchAnswers = () => {
        return $http.get("http://localhost:3000/answers")
            .then((response) => {
            return (this._answers  = response.data)
            })
        }
    this.fetchAnswers();

    this.getQuestionsById = (id) => {
        const questionsList = this._questions.filter(i => i.id_quiz === id);
        return questionsList;
      };

    this.getAnswers = (id) => {
        const answersList = this._answers.filter(i => i.id_question === id);
        return answersList;
    };
 
});

