const answers = angular.module("answers", ["answersService"]);

// CONTROLLER
answers.controller("AnswersPageCtrl", ($scope, AnswersService) => {
    const vm = ($scope.answersPage = {});
    
    vm.selected = null;
    vm.mode = false;
    // vm.editShow = false;
    vm.selectedElement = false;
    vm.student = {};
  
    vm.refresh = () => {
        AnswersService.fetchQuizes()
        .then((data) => {
          vm.quizes = data
        })
    }
    vm.refresh();
  
  
    vm.select = (id) => {
        AnswersService.fetchQuizes()
      .then((data) => {
        vm.quizes = data
      })
      vm.selected = AnswersService.getQuizById(id);
      vm.showQuiz = id;
    };

    vm.goToQuiz =()=>{
        AnswersService.fetchQuizes()
        .then((data) => {
          vm.quizes = data
        })
        vm.selected = AnswersService.getQuizById(vm.showQuiz);
        vm.mode = true;


        AnswersService.fetchQuestions()
        .then((data) => {
          vm.questions = data
        })

        AnswersService.fetchAnswers()
        .then((data) => {
          vm.answers = data
        })

        vm.questionsToQuiz = AnswersService.getQuestionsById(vm.showQuiz);

        vm.questionsToQuiz.forEach(element => { 
            element.answers = {};
            vm.answersToQuestion = AnswersService.getAnswers(element.id);
            element.answers = vm.answersToQuestion;
        });

    }

  });


