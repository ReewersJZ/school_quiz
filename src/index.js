// jQuery('button').css('color') // get
// jQuery('button').css('color', 'red') //set
const app = angular
  .module("myApp", ["quizes.service", "quizes", "answers"])
  .constant("PAGES", [
    { name: "teacher", label: "Teacher", tpl: "./views/teacher-view.tpl.html" },
    { name: "student", label: "Student", tpl: "./views/student-view.tpl.html" }
  ]);
// Global scope
/*
app.run(function ($rootScope) {

});
*/
app.config(function (PAGES) {
  // console.log(PAGES)
});
// local scope
app.controller("ApptCtrl", ($scope, PAGES) => {
  const vm = ($scope.apptCtrl = {});
  vm.title = "School quiz";
  vm.pages = PAGES;
  vm.currentPage = vm.pages[0];
  // vm.goToPage = (pageName) => {
  //   vm.currentPage = vm.pages.find((p) => p.name === pageName);
  // };

  vm.changePage = (pageNumber) =>{
    vm.currentPage = vm.pages[pageNumber];
}
});

angular.bootstrap(document, ["myApp"]);
